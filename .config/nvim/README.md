# Nvim Configuration

Plugins will automatically install after neovim is opened with this config.

## Dependencies

### Python

To install python dependencies run `pip install -r requirements.txt`

List

- Deoplete: https://github.com/Shougo/deoplete.nvim

- Pynvim: https://github.com/neovim/pynvim


### Fonts

Some are probably missing.

- Iosevka: https://aur.archlinux.org/packages/ttf-iosevka/

- Nerd Fonts: https://aur.archlinux.org/packages/nerd-fonts-complete/

