" ---				---
" ---	NEOVIM CONFIGURATION	---
" ---				---

" --- PLUGINS ---

" Install Plug if not currently installed
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
	!curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')

" -- Themes --
Plug 'joshdick/onedark.vim'
Plug 'catppuccin/nvim'

" -- NERDTree --
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" -- Tabs --
Plug 'mkitt/tabline.vim'

" -- Icons --
Plug 'ryanoasis/vim-devicons'
Plug 'kyazdani42/nvim-web-devicons'

" -- Status Bar --
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" -- Syntax --
Plug 'HerringtonDarkholme/yats.vim'
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'neovimhaskell/haskell-vim'

" -- Git --
Plug 'airblade/vim-gitgutter'

" -- Quotes and Stuff
Plug 'tpope/vim-surround'
Plug 'Raimondi/delimitMate'

" -- Commenting --
Plug 'scrooloose/nerdcommenter'

" -- Snippets & Stuff --
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'nvim-lua/completion-nvim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" -- LSP --
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'
Plug 'folke/trouble.nvim'

" -- Finders --
Plug 'wincent/scalpel'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }

" -- Motion --
Plug 'bkad/CamelCaseMotion'

" -- Folding --
Plug 'pseewald/vim-anyfold'

" -- Indentline --
Plug 'Yggdroot/indentLine'

" -- JS --
Plug 'yuezk/vim-js'
Plug 'Quramy/tsuquyomi'

" -- C# --
Plug 'OmniSharp/omnisharp-vim'

" -- LaTeX --
Plug 'lervag/vimtex'

" -- Writing Stuff --
Plug 'reedes/vim-lexical'
Plug 'ChesleyTan/wordCount.vim'

" -- Debugging --
Plug 'puremourning/vimspector'

" -- Ranger --
Plug 'rbgrouleff/bclose.vim'
Plug 'francoiscabrol/ranger.vim'

" -- Other --
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'prettier/vim-prettier', { 'do': 'npm install' }
Plug 'guns/xterm-color-table.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'vim-scripts/AnsiEsc.vim'

Plug 'mbbill/undotree'
Plug 'sindrets/diffview.nvim'
Plug 'szw/vim-maximizer'
Plug 'machakann/vim-swap'
"" Plug 'chentoast/marks.nvim'


call plug#end()

" -- SETTINGS --
"
set backspace=indent,eol,start
set ruler
set showcmd
set incsearch
set hlsearch
set mouse=a
set autoread
let g:tablineclosebutton=1
set number
set numberwidth=3
set relativenumber
filetype plugin indent on

let g:jsx_ext_required = 1

" Colors
lua << EOF
local catppuccin = require("catppuccin")

-- configure it
catppuccin.setup(
    {
		transparency = false,
		term_colors = false,
		styles = {
			comments = "italic",
			functions = "bold",
			keywords = "italic",
			strings = "NONE",
			variables = "NONE",
		},
		integrations = {
			treesitter = true,
			native_lsp = {
				enabled = true,
				virtual_text = {
					errors = "italic",
					hints = "italic",
					warnings = "italic",
					information = "italic",
				},
				underlines = {
					errors = "underline",
					hints = "underline",
					warnings = "underline",
					information = "underline",
				}
			},
			lsp_trouble = true,
			lsp_saga = false,
			gitgutter = true,
			gitsigns = false,
			telescope = true,
			nvimtree = {
				enabled = false,
				show_root = false,
			},
			which_key = false,
			indent_blankline = {
				enabled = false,
				colored_indent_levels = false,
			},
			dashboard = false,
			neogit = false,
			vim_sneak = false,
			fern = false,
			barbar = false,
			bufferline = false,
			markdown = true,
			lightspeed = false,
			ts_rainbow = false,
			hop = false,
		}
	}
)
EOF

colorscheme catppuccin
set guifont=Iosevka\ 14

" Spelling
set spell spelllang=en_ca
set nocompatible
filetype off

" Telescope
lua << EOF
telescope = require('telescope')
telescope.load_extension('fzf')
telescope.setup {
	defaults = {
		vimgrep_arguments = {
			'ag',
			'--nocolor',
			'--noheading',
			'--filename',
			'--numbers',
			'--column',
			'--vimgrep',
			'--smart-case',
			'--path-to-ignore',
			'.gitignore'
		},
		file_ignore_patterns = {"node_modules", "env"}
	}
}
EOF


" Tabs
set tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab

augroup pythonindents
	au!
	autocmd BufNewFile,BufRead *.py set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
augroup END

augroup csharp
	au!
	autocmd BufNewFile,BufRead *.cs set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
augroup END

augroup haskellidents
	au!
	autocmd BufNewFile,BufRead *.hs set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
augroup END

augroup jsindents
	au!
	autocmd BufNewFile,BufRead *.js set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab
	autocmd BufNewFile,BufRead *.jsx set tabstop=8 softtabstop=0 expandtab shiftwidth=2 smarttab
	autocmd BufNewFile,BufRead *.tsx set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
	autocmd BufNewFile,BufRead *.ts set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
augroup END

autocmd BufNewFile,BufRead *.md call DisableIndentLine()
autocmd BufNewFile,BufRead *.json call DisableIndentLine()
function DisableIndentLine()
	let g:indentLine_enabled=0
	set conceallevel=0
endfunction

" Undo File
if !isdirectory($HOME."/.vim")
    call mkdir($HOME."/.vim", "", 0770)
endif
if !isdirectory($HOME."/.vim/undo-dir")
    call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif
set undodir=~/.vim/undo-dir
set undofile

" LSP Install
lua<<EOF
local lsp_installer = require("nvim-lsp-installer")

lsp_installer.on_server_ready(function(server)
    local opts = {} 

    -- This setup() function is exactly the same as lspconfig's setup function (:help lspconfig-quickstart)
    server:setup(opts)
    vim.cmd [[ do User LspAttachBuffers ]]
end)
EOF

" Trouble
lua << EOF
  require("trouble").setup {}
EOF

" Invisible Characters
set listchars=eol:$,tab:>•,trail:~,extends:>,precedes:<

" Haskell Syntax
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

" Linter
let g:ale_sign_error = ''
let g:ale_sign_warning = ''

let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 1 

let g:ale_linters = {
 \ 'javascript': ['eslint'],
 \ 'typescriptreact': ['tslint', 'eslint'],
 \ 'typescript': ['tslint', 'eslint']
 \ }
let g:ale_fixers = {
 \ 'javascript': ['eslint', 'prettier'],
 \ 'typescriptreact': ['eslint', 'prettier', 'remove_trailing_lines', 'trim_whitespace'],
 \ 'typescript': ['eslint', 'prettier', 'remove_trailing_lines', 'trim_whitespace']
 \ }

let g:ale_pattern_options = {
\   '.*\.cs$': {'ale_enabled': 0}
\}

" C#
let g:OmniSharp_highlighting = 3
let g:OmniSharp_start_server = 0
let g:OmniSharp_server_stdio = 1
let g:OmniSharp_selector_ui = 'fzf'
let g:OmniSharp_selector_findusages = 'fzf'

lua<<EOF
lsp = require('lspconfig')

lsp.omnisharp.setup {
	on_attach=require'completion'.on_attach;
}

lsp.tsserver.setup {
	on_attach=require'completion'.on_attach;
}

lsp.hls.setup{
    on_attach = on_attach,
    root_dir = vim.loop.cwd,
    settings = {
      rootMarkers = {"./git/"}
    }
  }

EOF

" Airline
set laststatus=2
let g:airline_theme='minimalist'

" Snips and Completion
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
"let g:deoplete#enable_at_startup=1

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

set completeopt=menuone,noinsert,noselect

" Quotes
let delimitMate_expand_cr=1
augroup rulesdelimitMate
	au!
	au FileType markdown let b:delimitMate_nesting_quotes=["`"]
	au FileType tex let b:delimitMate_quotes=""
	au FileType tex let b:delimitMate_matchpairs="(:),[:],{:},`:'"
	au FileType python let b:delimitMate_nesting_quotes=['"', "'"]
augroup END

" Anyfold
set foldlevel=2

" CamelCase Motion
let g:camelcasemotion_key = ','

" Maximiser
let g:maximizer_set_default_mapping = 0

" Ranger
let g:ranger_map_keys = 0

" Writting
function! Prose()
	call lexical#init()

	let g:lexical#thesaurus_key = '<leader>t'
	let g:lexical#dictionary_key = '<leader>k'
	let g:lexical#spell_key = '<leader>l'
endfunction

autocmd FileType markdown,mkd,text,rst call Prose()
command! -nargs=0 Prose call Prose()

" Debugging
let g:vimspector_enable_mappings = 'HUMAN'


" LateX
call deoplete#custom#var('omni', 'input_patterns', {
      \ 'tex': g:vimtex#re#deoplete
      \})
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'

" Marks
lua<<EOF
-- require'marks'.setup {
-- default_mappings = true,
--  mappings = {}
-- }
EOF

" -- KEYS --

" General
nmap <Leader>vr :set invrelativenumber<CR>

" Saving, exiting and stuff
nmap <silent> <C-s> :w<CR>
nmap <silent> <A-q> :q<CR>
nmap <silent> <A-s> :wq<CR>

" Tabs
nmap <silent> <A-l> :tabnext<CR>
nmap <silent> <A-h> :tabprev<CR>

" Windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"  Togle NERDTree
nmap <silent> <F2> :NERDTreeToggle<CR>

" C# Things
autocmd Filetype cs nmap <buffer> <silent> <Leader>wg :OmniSharpGotoDefinition<CR>
autocmd Filetype cs nmap <buffer> <silent> <Leader>wd :OmniSharpGotoDocumentation<CR>
autocmd Filetype cs nmap <buffer> <silent> <Leader>wu :OmniSharpFindUsages<CR>
autocmd Filetype cs nmap <buffer> <silent> <Leader>wr :OmniSharpRestartServer<CR>
autocmd Filetype cs nmap <buffer> <silent> <Leader>ws :OmniSharpStartServer<CR>

" Search
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fs <cmd>Telescope git_status<cr>
nnoremap <leader>vl :let @/=""<CR>

" Trouble
nnoremap <leader>xx <cmd>TroubleToggle<cr>
nnoremap <leader>xw <cmd>TroubleToggle lsp_workspace_diagnostics<cr>
nnoremap <leader>xd <cmd>TroubleToggle lsp_document_diagnostics<cr>
nnoremap <leader>xq <cmd>TroubleToggle quickfix<cr>
nnoremap <leader>xl <cmd>TroubleToggle loclist<cr>

" Undotree
nnoremap <leader>uu :UndotreeToggle<CR>

" Git Diff
noremap <leader>ddf :DiffviewOpen --staged<CR>
noremap <leader>ddh :DiffviewFileHistory<CR>
noremap <leader>ddc :DiffviewClose<CR>

" Maximizer
nnoremap <silent><leader>vm :MaximizerToggle<CR>
vnoremap <silent><leader>vm :MaximizerToggle<CR>gv
inoremap <silent><leader>vm <C-o>:MaximizerToggle<CR>

" Ranger
nnoremap <silent><leader>rc :RangerCurrentFile<CR>
nnoremap <silent><leader>rd :RangerWorkingDirectory<CR>

" Anyfold
noremap <leader>vf :AnyFoldActivate<CR>

" Scripts
so ~/.config/nvim/scripts/visual-at.vim

" Reloads vimrc after saving but keep cursor position
if !exists('*ReloadVimrc')
   fun! ReloadVimrc()
       let save_cursor = getcurpos()
       source $MYVIMRC
       call setpos('.', save_cursor)
   endfun
endif
autocmd! BufWritePost $MYVIMRC call ReloadVimrc()

syntax on
