#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the process have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch main
polybar main &

external=$(xrandr --query | grep 'HDMI1')
if [[ $external = *connected* ]]; then
    polybar secondary &
fi

echo "Bars launched..."
