#!/bin/sh
if [ $(cat ~/.config/polybar/scripts/caffeine_poly | wc -c) -eq 1 ]
then
  echo "true" > ~/.config/polybar/scripts/caffeine_poly
  xautolock -disable
  caffeine -a &
else
  echo "" > ~/.config/polybar/scripts/caffeine_poly
  xautolock -enable
  caffeine kill
fi

